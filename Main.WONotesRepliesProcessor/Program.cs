﻿namespace Main.WONotesRepliesProcessor
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using System.Linq;

    using Main.ProviderRepliesProcessor;

    class Program
    {
        static void Main(string[] args)
        {
            Trace.Listeners.Clear();

            var logWriter = new TextWriterTraceListener(string.Format("Log{0:yyyyMMdd_H.mm}.txt", DateTime.Now)) { Name = "Log writer", TraceOutputOptions = TraceOptions.ThreadId };

            var consoleWriter = new ConsoleTraceListener(false);

            Trace.Listeners.Add(logWriter);
            Trace.Listeners.Add(consoleWriter);
            Trace.AutoFlush = true;
            var fromAddress = ConfigurationManager.AppSettings["Username"];
            var notificationRecipients = ConfigurationManager.AppSettings["NotificationRecipients"];
            var flags = (from fstring in ConfigurationManager.AppSettings.Get("ImapSearchFlags").Split(',')
                         select EmailsProcessor.ParseFlag(fstring.Trim())).ToArray();
            var emailsProcessor = new EmailsProcessor(Factory.GetMailProvider(), notificationRecipients, fromAddress, flags);
            var woNoteEmailDbHandler = new WONoteEmailDbHandler("WONotesRepliesProcessor");

            Trace.WriteLine("Reading settings from config file...");

            Common.ReadSettings(emailsProcessor, woNoteEmailDbHandler);

            Trace.WriteLine("The settings has been read.");

            Trace.WriteLine(string.Empty);

            emailsProcessor.MakeProcessing(woNoteEmailDbHandler);
        }
    }
}
