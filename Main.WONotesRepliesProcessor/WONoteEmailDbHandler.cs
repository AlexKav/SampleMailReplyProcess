﻿namespace Main.WONotesRepliesProcessor
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Text.RegularExpressions;
    using ProviderRepliesProcessor;
    using System.Configuration;
    using System.Linq;
    using Limilabs.Mail;
    using Limilabs.Mail.Headers;
	using Services.Core.InternalRequests.Workorders;
	using Services.Messaging.Workorders;
    using Services.Core;
    using Infrastructure.FormsAuthentication;
    using HtmlAgilityPack;

    public class WONoteEmailDbHandler : TracedEmailDbHandler
    {
        private bool actionRequired = false;
        private const string MailStart = "-- Reply to this email to post a work order note --";

        public WONoteEmailDbHandler(string marker)
            : base(marker)
        {
        }

        public override void GetEmailResult(IMail message, Action<string> logInfoMeth, out int trackingNumber, out string note)
        {
            trackingNumber = -1;

            if (message.IsHtml)
            {
                ParseHtmlNote(message.Html, out note);
            }
            else
            {
                ParseNote(message, out note);
            }

            if (string.IsNullOrWhiteSpace(note)) //if note still empty fill it from body
            {
                note = GetCombinedTextAndHtmlBody(message);

                //delete body of prev mail
                if (note.IndexOf(MailStart, StringComparison.Ordinal) > 2)
                {
                    note.Remove(note.IndexOf(MailStart, StringComparison.Ordinal) - 2);
                }
            }

            //If header start with Action Required than set Action Required = true
            actionRequired = note.ToUpper().StartsWith("ACTION REQUIRED");

            //If the user enters very long note we should cut it.
            if (note.Length > 4000)
            {
                note = note.Substring(0, 4000);
            }

            //Get trackingNumber from mail
	        foreach (MailAddress address in message.To)
	        {
	            foreach (MailBox mailbox in address.GetMailboxes())
	            {
                    //get all LocalPart of mail as trackingNumber
                    int.TryParse(mailbox.LocalPart, out trackingNumber);
	            }
                if (trackingNumber > 0)
                    return;
            }
            Logger.WriteLine(string.Format("\n\nmessage.To: {0}\n\n",
                       message.To
                       ));

            Logger.WriteLine(string.Format("Process current body. Tracking number is {0}, note is {1}, actionRequired is {2}",
                        trackingNumber,
                        string.IsNullOrWhiteSpace(note) ? "EMPTY" : note, actionRequired)
                        );

            if (string.IsNullOrWhiteSpace(note) || trackingNumber <=0)
            {
                Logger.WriteLine(string.Format("Record was ignored. Track number : {0}. Header : {1}", trackingNumber, note));
                note = string.Empty;
            }
        }

        public void ParseHtmlNote(string message, out string note)
        {
            var messageDocument = new HtmlDocument();
            messageDocument.LoadHtml(message);
            string body;
            if (messageDocument.DocumentNode.SelectNodes("//body") != null &&
                messageDocument.DocumentNode.SelectNodes("//body").FirstOrDefault() != null)
            {
                body = messageDocument.DocumentNode.SelectNodes("//body").FirstOrDefault().InnerHtml;
            }
            else
            {
                body = messageDocument.DocumentNode.InnerHtml;
            }
            var signatureRegex = new Regex(@"^(\n|.)+?(?=<div class=""moz-cite-prefix"">|<blockquote|<p class='MsoNormal'>|<div class=""gmail_quote"">|from:)",
                                            RegexOptions.IgnoreCase);

            //remove Signature and TimeStamp
            if (!string.IsNullOrEmpty(signatureRegex.Match(body).ToString()))
            {
                note = signatureRegex.Match(body).ToString();
            }
            else
            {
                note = body;
            }
            note = note.Replace("<br>", "").Replace("&nbsp;", "");
            
            //remove html tags 
            var noteDocument = new HtmlDocument();
            noteDocument.LoadHtml(note);
            note = noteDocument.DocumentNode.InnerText;

            note = note.Trim('\r', '\n', '-', '_', ' ');
        }

        public void ParseNote(IMail message, out string note)
        {
            var combinedBody = GetCombinedTextAndHtmlBody(message);

            combinedBody = Regex.Replace(combinedBody, "<[^>]+>", string.Empty);
            combinedBody = Regex.Replace(combinedBody, ">", string.Empty);
            combinedBody = combinedBody.Replace("&nbsp;", Environment.NewLine);

            var headerAnswerKeyWordsRegex =
                new Regex(
                    @"^(\n|.)+?(?=-original message-|wrote:|wrote ---- |writes:|\[mailto:|subscriber:|original message|" +
                                  MailStart + "|from:)",
                    RegexOptions.IgnoreCase);

            if (!string.IsNullOrEmpty(headerAnswerKeyWordsRegex.Match(combinedBody).ToString()))
            {
                note = headerAnswerKeyWordsRegex.Match(combinedBody).ToString();
            }
            else
            {
                note = combinedBody;
            }

            note = note.Trim('\r', '\n', '-', '_', ' ');
        }

        [Obsolete]
        private string RemoveSignature(string note)
        {
            note = note.Trim('\r', '\n', '-', '_', ' ');

            //RemoveSignature
            var signatureRegex = new Regex(@"^(\n|.)+?(?=thank|regards|sent from my|best|-- \n|-- \r\n)",
                                            RegexOptions.IgnoreCase);
            
            if (!string.IsNullOrEmpty(signatureRegex.Match(note).ToString()))
            {
                note = signatureRegex.Match(note).ToString();
            }

            //RemoveTimeStamp
            var timeStampRegex = new Regex(@"^(\n|.)+\n(?=\d\d|on )", RegexOptions.IgnoreCase);
                                    
            if (!string.IsNullOrEmpty(timeStampRegex.Match(note).ToString()))
            {
                note = timeStampRegex.Match(note).ToString();
            }

            return note;
        }
        
        public override string EmailProcessing(IMail message)
        {
            int trackingNumber;
            string note;
            string result = string.Empty;

            this.GetEmailResult(
                message, s => this.Logger.WriteLine(s), out trackingNumber, out note);

            if (trackingNumber > 0 && !string.IsNullOrWhiteSpace(note))
            {
                if (message.IsAutoReply())
                {
                    result = string.Format("ERR: Auto reply message (tracking number: {0})", trackingNumber);
                    Logger.WriteLine(result);
                    return result;
                }
                
                var сreatedBy = message.Sender.Address;

                var workordersService = new WorkOrdersService();
                var follower = workordersService.GetFollowers(trackingNumber, сreatedBy).FirstOrDefault();

                //Get user name instead email
	            var senderName = (follower != null && !string.IsNullOrEmpty(follower.Name))
		            ? follower.Name
		            : сreatedBy;

                //Validate user if allowAnyoneRespondNoteViaEmail = false
                if (!AllowAnyoneRespondNoteViaEmail(trackingNumber,new SubscriberService(null)) 
                    && !workordersService.ValidateFollower(trackingNumber, сreatedBy))
                {
                    result = String.Format("ERR: Validation failed (trackingNumber:{0}, CreatedBy:{1})",
                        trackingNumber, сreatedBy);
                    Logger.WriteLine(result);
                    Logger.Flush();
                    return result;
                }
                var newFollowers = message.To
		            .Union(message.Cc)
		            .SelectMany(x => x.GetMailboxes())
					.Select(x => x.Address.ToLower()).Distinct()
					.Where(x => !x.Contains(trackingNumber.ToString()))
					.ToList();

	            if (newFollowers.Any())
	            {
		            var requestToAddFollowers = new AddFollowersRequestInternal(
			            new AddFollowersRequest {FollowersList = string.Join(",", newFollowers)}, trackingNumber);

		            workordersService.AddFollowers(requestToAddFollowers);
	            }

	            //get from api mailedTo Addresses exclude sender
                var followers = workordersService
                    .GetFollowers(trackingNumber)
                    .Where(x => !x.Email.Equals(сreatedBy, StringComparison.InvariantCultureIgnoreCase))
                    .Select(x => x.Email.ToLower())
                    .ToList();

				//except new followers 'cause they already received the reply from sender
                var mailedTo = string.Join(";", followers.Union(newFollowers));
                Logger.WriteLine("mailedTo: " + mailedTo);
                Logger.Flush();

                try
                {
                    using (var connection = new SqlConnection(this.connectionString))
                    {
                        using (var command = connection.CreateCommand())
                        {
                            command.CommandTimeout = DbCommandTimeout;
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "dash_tbWO_notes_AddTicketNote";

                            //Always set as last note, You need to set this param if you want set note numeration.
                            //command.Parameters.AddWithValue("@NoteNum", NoteNum);

                            command.Parameters.AddWithValue("@Note", note);
                            command.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                            command.Parameters.AddWithValue("@CreatedBy", senderName + " (via email)");
                            command.Parameters.AddWithValue("@RecId", trackingNumber);
                            command.Parameters.AddWithValue("@MailedTo", mailedTo);
                            command.Parameters.AddWithValue("@ActionReq", actionRequired);

                            var outputnoteIdParam = new SqlParameter("@NoteId", SqlDbType.Int)
                            {
                                Direction = ParameterDirection.Output
                            };
                            command.Parameters.Add(outputnoteIdParam);

                            connection.Open();
                            command.ExecuteNonQuery();

                            result = String.Format("Ok: Note: {0} was added", outputnoteIdParam.Value);
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = String.Format("ERR: {0}", ex.Message);
                }
                finally
                {
                    if (String.IsNullOrWhiteSpace(result))
                    {
                        Logger.WriteLine("Message was not processed");
                    }
                    else
                    {
                        if (result.StartsWith(this.ErrorMarker))
                        {
                            Logger.WriteLine(result);
                            Logger.WriteLine("Stored procedure completed with error");
                        }
                        else
                        {
                            Logger.WriteLine(result);
                            Logger.WriteLine("Stored procedure completed with success");
                        }
                    }
                }
                if (result.Contains("Ok: Note:") && mailedTo.Length > 0)
                {
                    //Send Mails to mailedTo Addresses using AlertDispatcher
                    bool isTest = true;
                    Boolean.TryParse(ConfigurationManager.AppSettings["IsTest"], out isTest);
                    var dispatcher = new AlertDispatcher.Dispatcher();
                    dispatcher.SendCustomNoteAlertWithRecipientId(trackingNumber, mailedTo, false, false, isTest,
                        false, actionRequired);
                }
            }
            else
            {
                Logger.WriteLine(String.Format("ERR: TrackingNumber:{0} or note{1} is empty", trackingNumber, note));
                result = "ERR: TrackingNumber or note is empty";
            }

            return result;
        }

        public bool AllowAnyoneRespondNoteViaEmail(int trackingNumber, SubscriberService subscriberService)
        {
            var workordersService = new WorkOrdersService();
            var allowAnyoneRespondViaEmail = false;
             //Get SubscriberId by TrackingNumber
            var subscriberId = workordersService.GetSubscriberIdbyTrackingNumber(trackingNumber);
            if (subscriberId > 0)
            {
                //get feature flag
                var features = subscriberService.GetFeatures(subscriberId);
                allowAnyoneRespondViaEmail = features.Contains(ApplicationFeatures.AllowAnyoneRespondNoteViaEmail.ToString());
            }

            Logger.WriteLine(String.Format("allowAnyoneRespondViaEmail: trackingNumber:{0} subscriberId:{1} allowAnyoneRespondViaEmail:{2}", trackingNumber, subscriberId, allowAnyoneRespondViaEmail));

            return allowAnyoneRespondViaEmail;
        }

        public override bool HandleInternal(IMail message)
        {
            var result = this.EmailProcessing(message);
            return !result.StartsWith(this.ErrorMarker);
        }
    }
}