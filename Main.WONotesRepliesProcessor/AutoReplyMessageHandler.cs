﻿namespace Main.WONotesRepliesProcessor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Limilabs.Mail;
    using Limilabs.Mail.Headers;

    public static class AutoReplyMessageHandler
    {
        /*
         * header "auto-submitted" with value other than "no"
         * headers "X-Autoreply" or "X-Autorespond" with any value
         * header "Precedence" with value "auto_reply"
         * header "X-Failed-Recipients" with any value
         * Subject starts 'Delivery Status Notification (Failure)' (Gmail, Outlook)
         * Subject starts with 'Out of Office'
         * Subject starts with 'Automatic reply'
         * Ignore messages from fax, X-Mailer header with value RightFax SMTP/POP3 E-mail Gateway
         */

        private const string AutoSubmittedHeader = "auto-submitted";
        private const string AutoSubmittedHeaderUpper = "Auto-Submited";
        private const string XAutoreplyHeader = "X-Autoreply";
        private const string XAutorespondHeader = "X-Autorespond";
        private const string PrecedenceHeader = "Precedence";
        private const string XFailedRecipients = "X-Failed-Recipients";
        private const string XMailer = "X-Mailer";

        private static readonly IEnumerable<Func<IMail, bool>> AutoReplyRules =
            new List<Func<IMail, bool>>
            {
                mail => mail.Headers.Any(AutoSubmittedHeader) && !mail.Headers.HasValue(AutoSubmittedHeader, "no"),
                mail => mail.Headers.Any(AutoSubmittedHeaderUpper) && !mail.Headers.HasValue(AutoSubmittedHeaderUpper, "no"),
                mail => mail.Headers.Any(XAutoreplyHeader) && mail.Headers.HasAnyValue(XAutoreplyHeader),
                mail => mail.Headers.Any(XAutorespondHeader) && mail.Headers.HasAnyValue(XAutorespondHeader),
                mail => mail.Headers.Any(PrecedenceHeader) && mail.Headers.HasValue(PrecedenceHeader, "auto_reply"),
                mail => mail.Headers.Any(XFailedRecipients) && mail.Headers.HasAnyValue(XFailedRecipients),
                mail => mail.Subject.StartsWith("Delivery Status Notification (Failure)", StringComparison.InvariantCultureIgnoreCase) ||
                        mail.Subject.StartsWith("Out of Office", StringComparison.InvariantCultureIgnoreCase) ||
                        mail.Subject.StartsWith("Automatic reply", StringComparison.InvariantCultureIgnoreCase) ||
                        (mail.Subject.IndexOf("Auto-Reply", StringComparison.InvariantCultureIgnoreCase) >= 0),
                mail => mail.Headers.Any(XMailer) && mail.Headers.HasValue(XMailer, "RightFax SMTP/POP3 E-mail Gateway")
            };

        public static bool IsAutoReply(this IMail mail)
        {
            return AutoReplyRules.Any(rule => rule(mail));
        }

        private static bool HasValue(this HeaderCollection headers, string header, string value)
        {
            return headers[header].Equals(value, StringComparison.InvariantCultureIgnoreCase);
        }

        private static bool HasAnyValue(this HeaderCollection headers, string header)
        {
            return !string.IsNullOrWhiteSpace(headers[header]);
        }

        private static bool Any(this HeaderCollection headers, string header)
        {
            return headers.AllKeys.Any(x => x.Equals(header, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}