﻿namespace Main.Specs.WONotesRepliesProcessor
{
    using System.Collections.Generic;
    using System.IO;
    using System.Xml.Serialization;
    using Limilabs.Mail;
    using Limilabs.Mail.Fluent;
    using NUnit.Framework;
    using Main.WONotesRepliesProcessor;
    using Services.Core;
    using Subscribers;
    
    public class MailNoteAssociation
    {
        public long MailUid { get; set; }

        public string ExpectedNote { get; set; }

        public string MailCombinedBody { get; set; }

        public string MailTo { get; set; }

        public string MailFrom { get; set; }

        public string MailSubject { get; set; }
    }

    [TestFixture]
    [Category("integration")]
    class WoNotesMailRepliesProcessingTest
    {
        private static readonly string FilesFolderName = Path.Combine("WONotesRepliesProcessor", "Mails");

        private SubscriberService subscriberService;

        [SetUp]
        public void Init()
        {
            subscriberService = new SubscriberService(new InMemorySubscriberRepository());
        }

        [Test(Description = "MailProcessingTest.GetEmailResultByHtml")]
        [Category("integration")]
        public void IsMailProcessedTestHtml()
        {
            List<MailNoteAssociation> mailNoteAssociations;

            var x = new XmlSerializer(typeof(List<MailNoteAssociation>));
            using (var stream = File.OpenRead(Path.Combine(FilesFolderName, @"mailsHtml.xml")))
            {
                mailNoteAssociations = (List<MailNoteAssociation>)x.Deserialize(stream);
            }

            foreach (var item in mailNoteAssociations)
            {
                string note;
                var woNote = new WONoteEmailDbHandler("WONotesMailRepliesProcessingTest");

                woNote.ParseHtmlNote(item.MailCombinedBody, out note);

                Assert.True(!string.IsNullOrWhiteSpace(note),
                    "Note is empty");

                Assert.True(note.ToUpper().Contains(item.ExpectedNote.ToUpper()),
                    string.Format("Error. Uid={0}, ExpectedNote='{1}' Actualnote='{2}'", item.MailUid, item.ExpectedNote, note));
            }
        }

        [Test(Description = "MailProcessingTest.GetEmailResult")]
        [Category("integration")]
        public void IsMailProcessedTest()
        {
            List<MailNoteAssociation> mailNoteAssociations;

            var x = new XmlSerializer(typeof(List<MailNoteAssociation>));
            using (var stream = File.OpenRead(Path.Combine(FilesFolderName, @"mails.xml")))
            {
                mailNoteAssociations = (List<MailNoteAssociation>)x.Deserialize(stream);
            }

            foreach (var item in mailNoteAssociations)
            {
                string note;
                var woNote = new WONoteEmailDbHandler("WONotesMailRepliesProcessingTest");

                var email = Mail
                            .Text(item.MailCombinedBody)
                            .To(item.MailTo)
                            .From(item.MailFrom)
                            .Subject(item.MailSubject)
                            .Create();

                woNote.ParseNote(email, out note);

                Assert.True(!string.IsNullOrWhiteSpace(note), 
                    "Note is empty");

                Assert.True(note.ToUpper().Contains(item.ExpectedNote.ToUpper()),
                    string.Format("Error. Uid={0}, ExpectedNote='{1}' Actualnote='{2}'", item.MailUid, item.ExpectedNote, note));
            }
        }

        [Test(Description = "MailProcessingTest.AllowAnyoneRespondNoteViaEmail")]
        [Category("integration")]
        public void AllowAnyoneRespondNoteViaEmail()
        {
            var woNote = new WONoteEmailDbHandler("WONotesMailRepliesProcessingTest");
            Assert.True(woNote.AllowAnyoneRespondNoteViaEmail(12999501, new SubscriberService(new InMemorySubscriberRepository())), "Error in Mock");
            Assert.False(woNote.AllowAnyoneRespondNoteViaEmail(12999502, new SubscriberService(null)),
                "Error: check sub_id=2000059085 should not has AllowAnyoneRespondNoteViaEmail feature flag");//NOT DEMO

        }

        //just for testing by developer
        // before testing add mailsEml.xml to bin 
        //[Test(Description = "MailProcessingTest.EmailProcessingFromEml")]
        public void EmailProcessingFromEml()
        {
            List<MailNoteAssociation> mailNoteAssociations;
            var x = new XmlSerializer(typeof(List<MailNoteAssociation>));
            using (var stream = File.OpenRead(Path.Combine(FilesFolderName, @"mailsEml.xml")))
            {
                mailNoteAssociations = (List<MailNoteAssociation>)x.Deserialize(stream);
            }
            foreach (var item in mailNoteAssociations)
            {
                var woNote = new WONoteEmailDbHandler("WONotesMailRepliesProcessingTest");
                var email = new MailBuilder().CreateFromEml(item.MailCombinedBody);
                woNote.EmailProcessing(email);
                Assert.True(true, "Ok");
            }
        }
    }
}