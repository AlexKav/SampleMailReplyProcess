﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Service Channel">
//   Pavel Mironchik 2012
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Main,ProviderRepliesService
{
    using System.ServiceProcess;

    /// <summary>
    /// The program.
    /// </summary>
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new ProviderRepliesService() 
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
