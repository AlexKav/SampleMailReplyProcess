﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProviderRepliesService.cs" company="Service Channel">
//   Pavel Mironchik 2012
// </copyright>
// <summary>
//   Defines the ProviderRepliesService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

#define NOTESTMODE

namespace Main.ProviderRepliesService
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.ServiceProcess;
    using System.Text;
    using System.Threading;

    using Limilabs.Client.IMAP;
    using Limilabs.Mail;

    using Main.Infrastructure.Logger;

    /// <summary>
    /// The provider replies service.
    /// </summary>
    public partial class ProviderRepliesService : ServiceBase
    {
        /// <summary>
        /// The max per session default value.
        /// </summary>
        private const int MaxPerSessionDefaultValue = 100;

        /// <summary>
        /// The inbox folder name.
        /// </summary>
        private const string InboxFolderName = "inbox";

        /// <summary>
        /// The processed folder name.
        /// </summary>
        private const string ProcessedFolderName = "processed";

        /// <summary>
        /// The error folder name.
        /// </summary>
        private const string ErrorFolderName = "error";

        /// <summary>
        /// The error marker.
        /// </summary>
        private const string ErrorMarker = "ERR:";

        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILogger Logger = new MainCompanyLogger().GetInstance("ProviderRepliesService.log");

        /// <summary>
        /// The process mails.
        /// </summary>
        private Thread processMails;

        /// <summary>
        /// The should stop.
        /// </summary>
        private volatile bool shouldStop;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProviderRepliesService"/> class.
        /// </summary>
        public ProviderRepliesService()
        {
            Logger.Info("Create service...");
            this.InitializeComponent();
            try
            {
                this.Hostname = ConfigurationManager.AppSettings["EmailHost"];
                this.Port = int.Parse(ConfigurationManager.AppSettings["EmailPort"]);
                this.UseSsl = bool.Parse(ConfigurationManager.AppSettings["UseSsl"]);

                this.Username = ConfigurationManager.AppSettings["Username"];
                this.Password = ConfigurationManager.AppSettings["Password"];

                this.CheckInterval = int.Parse(ConfigurationManager.AppSettings["CheckInterval"]);
                this.PathToMailFolders = ConfigurationManager.AppSettings["PathToMailFolders"];

                var maxPerSessionConfigValue = ConfigurationManager.AppSettings["MaxEmailsPerSession"];
                this.MaxEmailsPerSession = maxPerSessionConfigValue != null ? int.Parse(maxPerSessionConfigValue) : MaxPerSessionDefaultValue;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw new InvalidOperationException("Wrong configuration. App.config should contain the following settings: Pop3Host, Pop3Port, UseSsl, Username, Password,CheckInterval.");
            }

            try
            {
                var connectionString = ConfigurationManager.ConnectionStrings["MainCompany"].ConnectionString;
                this.ConnectionString = connectionString;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw new InvalidOperationException("Invalid connection string setting.");
            }
        }

        /// <summary>
        /// Gets or sets the hostname.
        /// </summary>
        public string Hostname { get; set; }

        /// <summary>
        /// Gets or sets the port.
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether use ssl.
        /// </summary>
        public bool UseSsl { get; set; }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the max emails per session.
        /// </summary>
        public int MaxEmailsPerSession { get; set; }

        /// <summary>
        /// Gets or sets the check interval.
        /// </summary>
        public int CheckInterval { get; set; }

        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Gets or sets the path to mail folders.
        /// </summary>
        public string PathToMailFolders { get; set; }

        /// <summary>
        /// The do work.
        /// </summary>
        public void DoWork()
        {
            while (!this.shouldStop)
            {
                try
                {
                    using (var imap = new Imap())
                    {
                        // Use overloads or ConnectSSL if you need to specify different port or SSL.
                        if (this.UseSsl)
                        {
                            imap.ConnectSSL(this.Hostname, this.Port);
                        }
                        else
                        {
                            imap.Connect(this.Hostname, this.Port);
                        }

                        // You can also use: LoginPLAIN, LoginCRAM, LoginDIGEST, LoginOAUTH methods, 
                        // or use UseBestLogin method if you want Mail.dll to choose for you.
                        imap.Login(this.Username, this.Password);

                        // You can select other folders, e.g. Sent folder: imap.Select("Sent");
                        imap.SelectInbox();
#if TESTMODE
                        var fl = Flag.Seen;
#else
                        var fl = Flag.Unseen;
#endif
                        // Find all unseen messages.
                        var uids = imap.Search(fl);

                        // var status = imap.SelectInbox();
                        Logger.Info("Number of unseen messages is: " + uids.Count);

                        int cnt = 0;
                        foreach (long uid in uids)
                        {
                            if (cnt > this.MaxEmailsPerSession)
                            {
                                break;
                            }

                            var email = new MailBuilder().CreateFromEml(imap.GetMessageByUID(uid));
                            Logger.Info(string.Format("Processing message {0}", this.CleanFileName(email.MessageID)));
                            try
                            {
                                // put to inbox
                                var inboxFile = Path.Combine(
                                    this.PathToMailFolders,
                                    string.Format("{0}/{1}.eml", InboxFolderName, this.CleanFileName(email.MessageID)));
                                this.CheckDirectory(inboxFile);

                                email.Save(inboxFile);

                                // process message
                                var sucess = this.ParseInDatabase(email);

                                if (sucess)
                                {
                                    // put to processed
                                    var processedFile = Path.Combine(
                                        this.PathToMailFolders,
                                        string.Format("{0}/{1}.eml", ProcessedFolderName, this.CleanFileName(email.MessageID)));
                                    this.CheckDirectory(processedFile);
                                    email.Save(processedFile);
                                }
                                else
                                {
                                    // put to error if any errors occured
                                    var errorFile = Path.Combine(
                                        this.PathToMailFolders,
                                        string.Format("{0}/{1}.eml", ErrorFolderName, this.CleanFileName(email.MessageID)));
                                    this.CheckDirectory(errorFile);
                                    email.Save(errorFile);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error("parse in db error", ex);
                            }

                            Logger.Info(string.Format("Message {0} processed", email.MessageID));
                            cnt++;
                            Thread.Sleep(100);
                        } // foreach

                        imap.Close();
                    } // using
                }
                catch (Exception ex)
                {
                    Logger.Error("process error", ex);
                }

                Thread.Sleep(1000 * 60 * this.CheckInterval);
            }
        }

        /// <summary>
        /// The on start.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        protected override void OnStart(string[] args)
        {
            Logger.Info("Start processing...");
            this.processMails = new Thread(this.DoWork);
            this.processMails.Start();
        }

        /// <summary>
        /// The on stop.
        /// </summary>
        protected override void OnStop()
        {
            this.shouldStop = true;
        }

        /// <summary>
        /// The clean file name.
        /// </summary>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        /// <returns>
        /// The System.String.
        /// </returns>
        protected string CleanFileName(string fileName)
        {
            return Path.GetInvalidFileNameChars().Aggregate(fileName, (current, c) => current.Replace(c.ToString(CultureInfo.InvariantCulture), string.Empty));
        }

        /// <summary>
        /// The check directory.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        protected void CheckDirectory(string name)
        {
            var dir = Path.GetDirectoryName(name);

            if (!string.IsNullOrEmpty(dir) && !Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
        }

        /// <summary>
        /// The parse in database.
        /// </summary>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <returns>
        /// The System.Boolean.
        /// </returns>
        protected bool ParseInDatabase(IMail email)
        {
            bool result;

            try
            {
                Logger.Info(string.Format("Handling message {0}...", email.MessageID));
                result = this.HandleInternal(email);
                Logger.Info(string.Format("Message {0} handled successfully.", email.MessageID));
            }
            catch (Exception e)
            {
                this.HandleError(e);
                result = false;
                Logger.Error(string.Format("Message {0} handled with error.", email.MessageID), e);
            }

            return result;
        }

        /// <summary>
        /// The handle error.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        protected virtual void HandleError(Exception exception)
        {
        }

        /// <summary>
        /// The handle internal.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <returns>
        /// The System.Boolean.
        /// </returns>
        protected bool HandleInternal(IMail message)
        {
            Logger.Info("Calling processing stored procedure...");

            var body = this.GetCombinedTextAndHtmlBody(message);

#if TESTMODE
            var result = "OK";
#else
            var result = this.CallEmailProcessingStoredProcedure(body);
#endif            

            if (result.StartsWith(ErrorMarker))
            {
                Logger.Info(result);
                Logger.Warn("Stored procedure completed with error");
            }
            else
            {
                Logger.Info(result);
                Logger.Info("Stored procedure completed with success");
            }

            return !result.StartsWith(ErrorMarker);
        }

        /// <summary>
        /// The call email processing stored procedure.
        /// </summary>
        /// <param name="combinedBody">
        /// The combined body.
        /// </param>
        /// <returns>
        /// The System.String.
        /// </returns>
        protected virtual string CallEmailProcessingStoredProcedure(string combinedBody)
        {
            using (var connection = new SqlConnection(this.ConnectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "MailDispConf";

                    var bodyParam = command.Parameters.AddWithValue("@Body", combinedBody);
                    bodyParam.Direction = ParameterDirection.InputOutput;

                    connection.Open();

                    command.ExecuteNonQuery();

                    return bodyParam.Value.ToString();
                }
            }
        }

        /// <summary>
        /// The get combined text and html body.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <returns>
        /// The System.String.
        /// </returns>
        protected string GetCombinedTextAndHtmlBody(IMail message)
        {
            var combindedBody = new StringBuilder();

            var textMessage = message.Text;

            if (!string.IsNullOrEmpty(textMessage))
            {
                combindedBody.Append(textMessage);
            }

            combindedBody.AppendLine();

            var htmlMessage = message.Html;
            if (!string.IsNullOrEmpty(htmlMessage))
            {
                combindedBody.Append(htmlMessage);
            }

            return combindedBody.ToString();
        }
    }
}
